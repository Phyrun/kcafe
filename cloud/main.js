var _ = require('underscore');

Parse.Cloud.define('hello', function(req, res) {
  res.success('Hi food app API');
});

//=============================
//            USER
//=============================
Parse.Cloud.define('get_users', function (request, response) {
    var query = new Parse.Query(Parse.User);
    query.find({
       success: function (user) {
           response.success(user);
       },
       error: function (error) {
           response.error('ERR-QUERY-USER')
       }
    });
});

Parse.Cloud.define('get_single_user', function (request, response) {
    var users = [];
    var userId = request.params.userId;
    var query = new Parse.Query(Parse.User);
    query.equalTo('objectId', userId);
    query.find({useMasterKey: true}).then(function (user) {
        _.each(user, function (u) {
            let userData = {
                objectId: u.id,
                firstName: u.get('firstName'),
                lastName: u.get('lastName'),
                email: u.get('email'),
                image: u.get('image').url(),
                phone: u.get('phone'),
            }
            users.push(userData);
        });
    }).then(function () {
        response.success(users)
    });
});


Parse.Cloud.define('sign_up', function (request, response) {
    var firstName = request.params.firstName;
    var lastName = request.params.lastName;
    var email = request.params.email;
    var password = request.params.password;
    var phone = request.params.phone;

    var base64 = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUSExMWFhUXGBUaGBcYFxgdHxoYGhUXGBodHx0YHSggHR0lHhcXIjEhJSkrLi4uGB8zODMtNygtLisBCgoKDg0OFxAQFysdHR0tLS0tLS0tLSstLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAAAQYHAgQFAwj/xABEEAACAQMCBAQDBQUECQQDAAABAgMABBESIQUGMUETIlFhB3GBFDKRofAjQlKxwTNystEkNDVikrPC4fEVQ1OCY3N0/8QAGAEBAQEBAQAAAAAAAAAAAAAAAAECAwT/xAAeEQEBAQACAwEBAQAAAAAAAAAAARECIRIxQVFxA//aAAwDAQACEQMRAD8AtXHelmjFFeN6DxQKM+9Oilj1p5pUCgdOl7UUBSFZUs0Cx7UZoNBoAGjNKl70DzRmo9xfm+2gYxks8g6pGuoj59h9TXLi5nu7na1tkXOfNLIMjB6lBvj2q4ibZpCuBwu3vwUM00LDPmVY2Bx/utnr8xUgJqAopCigyzRmkKWaDLNApUUGVIUYo+W9A8fWlilWQNAsGijf1FKgxZfessUqKKyFIUZp4oDNI4p/Sg0CFZUqWKDKkaRFaHGeLRWyB5DjJCqo3LMewHeg3mYDfNRLivxCs4kYo/iupICKDud+52x71FPiJzwzE2tu2lMYkYdcnquewHQ/hUAjddipUkg/e/dxjzHse+B7VucU1P8AiHNryReNPcvbhtWi3iUF2XBwSzdATkZOOlcOPmi5MIXxzBbgncsWlk9QD1229AM9aiInGSSSfc/96knBrCIReKdLyDd5XP7KFewI/ffPbpnFayRGxf3niIFjH2S2OSZZCTJMQN848zZyPKNq8Le84fCMx/bPGAOHBC7469c461wJzqdnk8Q5HlLdSM5zj09hW3wNGlYwrIkYf78kmBt6ajvgnAwOtXBNOD84cUZEVI1lVmCLKyHcnfcggHHrjtUng53EMohuzEW3y8LFlT0DDGQagt/zCYhpV5LjwsDUrGOFMeXy6BluuOtcjhfMN4i6IEXBJc/sgxI6nJO5X9ZrOK+g4ZldQ6kFSAVI6EHoa9N6qDg/xCnTQXlt3XIUxaTGwG26nOn6GrW4depLGkiEFWAI+v8AX2rNmDZFOgGnUUsUZFGaBURlSzQaKB5pZ/WKCKM0Dyf0KVGaKKWKMUUsUDFCmjNFA6KCM0qAzSzWRFc7mG7MVtNIOqoxB9Djb86DG14wkk7wIrN4Y87jGkN/DnuflVW818RN9eOELCO3VyCP4UB1Ee5bA/CpDzBx6OwslgiObiVNgPvanG7tnvk96jcUCh5bXWDMbIxjQNg4AfAPckAk+h2rcgg14gRygOOin+v51kVQIdzknBGOg9c9/lW1ccHnKySNGQuEIfBCnUQNvUnPSpfP8PLv7OkeVdCBIdIAbxDhSN9yAu46d63rOIDLEGLMg0qACB1OnpqP671IOMcTimaOGIGO1gj1AFd5HwMsw7ljgb+9TvkLla2kgmjnUPMC8UnUMqYAVT/w52rnXnwqnXUsUyOhOwbI0gdM9j6VNi4rO/vXmdpG6k9v5AegxWNpaySkqoHlVnYk4ARRuT+u4q4YfhTF9nCs/wC2BLB1GNyuNJ9QDvmtcfCthbvELgB3dSW0fuqNh69d/oPSnlExUni+TTqbqNKD7uOpPXrnHavW/wCKTSMXZiGwqnR5QVAAxhe2BUpPJksTTROjNKqjwlVSRJk4JDdgBue9a19ybOJo7VAGnKa3UHZAfVunTf61dhjl8vSyKWMcsSbeYyBTkZ6AEEn6elTP4ZcfeO6+zBleOVm7EYYKTlR2Bx0qHXFiqI+lQ2ltLSlsDV6IvUjY710uSrs206XjQyNCuVLAA4Yrg/kenvSj6BzWQNeVtMHUMOhAI9cEZr0zXJoxTpUxQGadGaM1AA0UUh+s0DzSp4NKqEKdKnqqAH6xQKDRigY/Roo2ooCuHzsubG43C4jY57eXf88V3M1CudprqUyWcNuDE0XnmdsKuc7g+2nce9WCnxMtzOHuJGAYqGl66SehYfw7HpVucn8nQApdEozgkqYmOg4JAcehIzkdN6gHL1qDJH4o/ZIwj1wRB0fzZ87dx7+lXnbxKihVUKo2AAwBW+VI8eI8LinTw5UDJkMB7qcg7VtjHSg0VhSCAEkAAnqQMZ+frWQpE0GgKdY0UDI7+leKWaB2kCKGcAMwG5A6An0r1ozQVV8QeULVXaeSYxmQ4jjWPOSF6eXfc/zrXk5YbNo0XjQROpabrpQoNmKFsAHGd87VaHFPBVfGnC6YvOGYfdOMZ+dVhJcm6gubma6uDGH0mOJMKULA4GrcjfBPb3rcrKyOA8US4j1x6tKnSGK6dWAPMvqp9a6eaifLF7NMlubfQLRUw2reTK5GnA2H7p+VSwVmqZpCnQTUBSxQBQRUGVGaVG9A/rRS3oogY0Y+dIGmTRSAp4pCgGgdOkTSxRQ5wM5qBwXAnkurmC5M5AKLbSZRB2YEfvAgHB+dTW9tVljeJ/uupVsEg4Iwdx0qsOCXkZjurJ4hIkb/ALOIOxlcq4G2r90deu29aiJHy7wuT7TKHnRY41CrbQNhVBB++Oud6lPCuGpbp4cedOSfMxY7+53rXEcFsstwVWMFQ0jYPRV6n3AqueP/ABXOWW1jGMjEj9x38u350zRal3cpGuuRwi7DLHG5OB1rl8V5otbckSygMACVG5ALBQTjpuf51SPE+Zb6YGGWR/NIsgXBBBydIXvpz0HqK2RyTxFonm8I+fZlJ8584OdPfcZ9etXxNXHa81Wsk6W6Sh3dSy43UgEgjI77H8K7WagfLnw/S3nguUkYFVy0b4J1lcdR06n1qc6qlV6E+9RbmDnq0tJfBlLl8AkKucZxgH5g5+lSbVVdc6fD1rmV7iKYB2wSr5wT06joAAMbetJP1K9eF/EeCW4cvIYYETyqy7uerEkZ6dlHWpNwfmy0uWKxTKWGolT5TpHU4PaoAvwoPU3aADvo9vc+vv0rXT4dsqlo72EvjB7DcaWGoH3O3etZEW6wSRMHS6MPYgg/karzivK8VnHL/rE32nMSqg/swx19PTI/CtrkLle5t5fFkmXwwjKIkZioJI3wTgbLnbPWpFznYST2jpFkybFQHK7g+o/HFT1VcPky8jDQW8kjJcwo0ZhB8pA/eIXbVgDrv/Wcn8qg/K188At4JLSQSSAiSUaTlgSMsc5Ixg5PqKmwqUjKgUZoqKYNFI/KigYopGgmoh0UsUUBRQKM0BmnppUxRRWLmsqiPGeLzy3osLZvD0prlmwCQp6AA7Z3H41R1+aOIC3tJpiC2hGOASCc7dR069e1VbypEjxwvcC5ZzI3gPHtoXILM0p2IyTnUe1TWG2kTxGtrp7rQSJbeQqc+oBIBU9cdj0qmuM3xMskMJlWEOzLExPlLYJUr6htse1a4pVr82fEi2gLQCMz+UglWXTuvQnPvv8AWq2s+aIYSWhsom3yPFJcjrjc49fyFRy5t2UkOGViA2koQQfqNxjf61anKvKZmtLeZbS0OpRuzTam7ZbGx77VrJERqb4iXTSeL9ng1gABjFkjHTcnPftWdz8R+JlQSwUHONKL/XPrU3blySPIZeGRZ6ZjY9sfvsK9+HcFaQAwycOfTgaltwcEb9m65qbPwxWb/EHiONJuGHfOlc9OmcV62XNHFLmRYEuW1vkLlguTgnqB122q2o+DXYBH+g9uluw/6qqXnKWReJsIVAljMYHhpjzqoOVUZ9fyqy6N295b4nHbvdXN28Sjcq0srNknA2XI3Jrm29lcvM8DXaKwCEGWVgH1KDsd+invXhxS5v3DCd52GckMHxn5HYVNvg1w+KaK48WJZAHQDWoOPKcgZFPgjA5au2wv2u10knrcjG3fHvtXjJyvc+bEtqwx1W4QAgegOPzq4eIpwy1GJVtkz+6VTUfpjJrW4TxLhNwdMQty7bBTGFY432DAZqaqoze39lFpS48JSxGhJUJzjrhScA46+1du2+IV99nCOEYMpXWSAxyMZ2Oc/Sux8Z7CGK2hEUUaFpDuoUHAU7YHXqK0m5CtYLRLm5nlYMIj+zQYXUB23ON+tXpG9y1IttbwFYXlvZGfwkZ84U+UuMbBDXauuKcTtdNxcrFJB/7iRjzRg+h74+tRbkLjEFqkjIr3Fw0pjiQBgwj6r12CnrsKn3O9+Y+HSsy6WdAukEHDPgYztnHrUqu/w69WaNJUOUcBlyMbH2NbAFc3liwMFpDCdyqKD88ZP5muoRWKF+FFOigWKdKkKA/XWnT+n5UUMYZp0s081A80s0Ypk0UVBeKkWnGIZ8NoukMT4OfOCuk49Pu1OgKjPP3BmntxJFkTQMJI8DqR1H1/mBVg1OeJEtit7HKsc65ARiAs6jdkPvjofXFV/wArNNf8SuJ4JBah9Dv0J2wMDI3OVJqxuGTQcVsdMwBbGmRdgySDqR3HqDVecH4bBb8Xmt1gefS6CJNXlXbLM+Qc4B2rcR1PjfbgG2fYkrKhPc/dI/r+NSD4b3LTcIVIW0yIJIwdjpbJKk+24Nanxk4c0sMDIhYrIw8ozsy+g91FVjY8QuLbL285iVdBMZc5YlQCdJ2bf16bU9wW9w/4eWujNzrmmIGt2dtjjfGMbZz19qI+QYobmK4tZGhCsNcYJIdRn1Od9hvkVV0XxI4kp/tgx22KqfyA716XXP8AxOdfD8QKd8+HGQxHTtnA37Yp401afN3PlvZqygiSUbaFP3TjbUe3y61XPwxinueJfa9OVBdpHxtlgRgZ7/LOMUcicj3FxOs10n7FGywk1Au3Xp3GeudjjG9WlzDw66MaR2LxwAth207hMbaQBj9dqdQdXjEBeGSMfeZHC4IByVIqjrO74hwWQao8RyHJVsFHIGPvDow271Y0vw5Rl1G6uDcf/OWOQflnp261p8DvMyTcL4k0cnhgFHcgalOMdf3sEEHr+FIVnyVFw67L3QAed3JZJmBZDtkAHqvocV3OIcrWMkqzNGivGQ2VbTuDkagPfeqsu/h1dfaZVt4z4at+zk1AZU7qQ2d8f0retPhPeOT4kyR5+8dTOT79s/U96dfo9/i3xQTXdvbggomCSDnJdgO3oB+dWNzLaarYRLbeOh0q0YYLhANiudsjAxUX5Z+FsdtMs0kxk0EMihdI1D13OfXHtUt5ntfFiEQuDAXYAMpALHBOn3z6D0qUVx8JrQLeTs6eHoyiKxHlctuu53YAdvU1IucQt5e2lmjagrNJMFOwAxgEjvsRj/e96jPKlhFA3EL+Q+Ibd3WLxO7/AMRx3JIA+e1Tb4dcHEVuLh8Ga4/aM3cBtwufTcn61aJci47UGlWWawMaeaM0ZoAGnWOR/wCKDQZYFFLHz/OigxBpgUjSqBketMAVjTzVUxQQaQp6qCs+cbGTh90OIWxCrKwWVD90sSTv6A/kfnWvy/diXjtwyzquQuy4IkCKuUz7eo9DVk8U4bFcRmKZA6HBIPqDkHIqqE4LZWvG9DkRQxosia3IGvSpHmJz11HGe1alRcWB3Fa89jE+zxo395VNe4bbI6U9VZV4Q2MS40xIuPRVH9KyFsoJYKoY9TgZP1/D8K9tVc+64zBGzK80aMgBYMwBAPc5PT3oIbzqr3l5Hw5JDEBGZWfP3iQQowCCwBG4/wAqndpFoRV3OlVGT3wMVEOauHWd46st0sVwi+R0kXIGruAdxk+vevXlHiZjd7S4vUnlUZXCnZRsQW6Fuhx1FX4iXO4AyTgAdSarHhdlb8Rvr65mjD26gKjnYAqoBYH5AnPoRUh534JbyD7Rc3E0USABlV8I2+3l9Tkjbfeoxf8AG+EyQi2DTRRRgsujKCXSMFDk+YnbrVgkXwouC1myk5RJZFjPqmxHX3JqaZFQPg/PfCoIo4om0Jt5dDeXVknO25z1O/UVJrDmO3mlMMT62VQxIBKjPYt0De3XepR1iKjnNb2LNFBedWJMYIbGrZeq7ZOcYqRfXrUT+JXDZp7QiEqNB8RtWc4QFsKQNjkCkFeI6pw2eEHeS90oB3C6c53z/wCRV12kYVFXGNIAx8gBVUfC/lb7Qkd7LKXVZJCIiMgsDjVvt136dhVuVeQeaAaM0jWQ8+lIGinQAoooxQLH63p0sn9Cigxp6qWaWaDLNL3yaVMUDzT/ADpUUU6qz43cOUC3uVH7TVoPvGAW3+Rz/wARq0yfeuLzHwJbkxSHOqEuyjbDakZdJz2yR+FJexFOWuYG8KW9GpvHljit7ct2XSm3Yd8np5antteJJqMbhtLMrY7MvUfMVRvLUMdhfJFxA4EK+JGBqYLI+MdPln0yM1b3CYxaQftHTSPElkk6DzOWzn69TvsKtg1uZObPsU0Ylhc27g5nXfS38JUew/P51E+YOXLTik5mt7+PUQNaEhjsMKQMgge3rVkW08c8SuuHjkGQcbEH2PauBxP4f2EwJ8HQx/ejJUg+uOn5UlFecb+FU8QJt5PFXTnB2YsCMADofmTS4R8L7thqkcR+YAjOToIBZtj17YPpUxk5EuEH+j8TuF2Aw/m2HQZBGPwry/8ARuOJjTewyY7Mo327+WteSY7fO3CJJ7CWCEnXpXAyPNpIOnJ9cVWPD/hdeybOVjXORqOcAgHYKTv6/KpkTx8dPsx99vT59KxXgXGLhsT3iQp/+IbnPbbBGPUmpLginEeSrO0lU3N5qiwcqmPELg9NKg4X3616WdlLeKLXh8bxWYfLzMSGduhJP72B0UdNs1POD/D2zh8zqZ5DuzynOT1zp6Z99zUqjRVACqAB2G2PoKeRjG1hCIqDcKoA+QGKhfxD4u4jljhaRJYVWY9NMkRJVhvuQM5I9qkHFeIBp0svMPGjl/aK2CrLp2GOhw2fpVX/ABD4muYo0M4uItdu3iganjdSNWQd8/8AXSCVfBWcmwZf4Znx9QrfTrU/H6FR/kLhj21jDC64dQ2obdS5PUdetSLNS+wVjT96dRSFBNI0gKIYp1iBWQoHRSz+tqKgwpiscUZqhk06WfxozQOnmlSorMCgfresQPSnQVd8VuVlEdxe6iXYwBRj7mDpbBz0ORtjavF+bft1ubF0WFzDJ4ztnEZiwc6cfdOB7jPerO4lw+OeNoZV1I4AZfXBz2+VU98VeX3ju4zbIw8WNY8ID52GRpP8RKhfngVqdonF7x1g1jZ20qmSTwmdwpZfACHLeg1acDfuKmQqsOBcywPN9qEXh/ZrURzMS3kOrATRjcal6joM59KlHJMrpZCW4OCxllJLE4RmLjr2ANSxUorj84RStZzCDV4unyaTg5BB2PyresL1Jo1ljbUjjKnfcf0rZzUFScE5pvx4sbyamjWbUJFUMumMFSTkYGrqSD29a6fw3v7u5uHlmudcaoAUBXBYjAOANsAHI9TUsk5TtWuHuWj1O6sGycqcgKdvkMfU107OyjiGmNFQbfdAA2GBn12rVqNnNec7dgRqION/8u29ehquuNcciPF4CGLCBJ1fCscPoLnGBucDt6fhIrz5f4w0UN/cuAbxH1TRtsAFAUFR106R1PpXL4TB/wCq8VF4YpBbIq6WIwNcRUgHGxySdvl6Vx7ie54tOBblQ8kGmfTlVADsyhz67Lkb77VcPLHCxa2sMAAUog1AHIL4yxyeu+a16R1RTxWOaNVYGRFYmijNFLNOsQay1UBmjNY4ozRD39qKNdFQLFFYk0aqBmlmimv41QgKeKDSBorMUZpUlaoMjXN4rwWKeSGRwS0L60Oe+Mb+o6H6V0QaDVFL81cpzlb+5RZVfx2ygG0sBIbUMbkDJJ69O1afLfNebC4tSZZbq4bQi9QQyhBgk7HA/lV2cQ/sZMfwP/hPY1RXKnKovUjEewDapJNSllOrBXSPMo04IO4ye2a3Ls7RbPEOK/Yo7SFIS2tki0hgCqhdzsN8AZNbXLHMSXkRdRoIZkZCQSCuPTrsQdvWqJ5hlurW6IM8jmB3RCzEsABsd8gZVgfqa7nK89vGvDZi6I/2i4MjFxhEwFGoZ8oIAx8qXiavN5AASSAB1JOwrS4nxiC3QSTSKkbEKGJ2JO43GcDbr0qkoZFZo5JbkvBJeTBg7EBowUBc791OPasLLmNRw64tDKNPixeEjYyIy+W0k9th64yfWp4mri4xzba26K7yqQwVl0kMSrdGGNyu3UVWU3NBmmjisnCSXLu0ryomqFmCjKyYAwEU++B6kYiHGeMLJDDbxgnwmn83qryAoBnfAA/OpDyBygtxOi3MbmMh3yCcMY2KaCQMjck9ew2qyYmpdyPwxOH372yyeKlxCrpJkYLITqGx9ye/TrVlDFVxxrkGO1gNzaPKZ7c+LHrbUNIySoXAG/Xpnappy5xhbq3jnUY1rkj0YbMPxB/KpVdTIoGKx/WKDWQZ9sUyaWaWaB6qCaKWaB5opUUBmijPzoqBUUqMfSqGxpU9qWPegeqkaDvSFFZCg+9Y5oLAUGQNMmuXxjjEduBnzSN/ZxLjU7eij+tRW15zV5HtridYJZcrGsfm8E7rh3+74me3QYpg7nM/MkEUFwPERpY4nJjDebpgZAOepH41x/hhy8YYFmlihEjqCjqp8TQw1YYnbO4G3pUTtOL2lvIR9kSaBSYZ5ZFBuC5yWZkc5Kkjp2wfarC4Pztw+YAR3MYONlY6CMezYrWdI4/FuRWl4h9qBXwmOWB6jMJjJ0lTkghGBzjc7d6gkfDoobxIHgWV4Lho99KiVHiLRBtvv5XOT/F+FyJx+1YkC4hJA3/aLt19/Y1rW1nZSStcIYneTwySGU5ZMlGAB+8OxFJTFWcE5WEssatasssE4FxE2CDDNqZGOdsr0J+R6jbr8t/DuSKO8Wa3hlkKqLdnIK5OrfOMgjK527VZr3ECM0jNErkKGYlQcb6QSd+5wK8r3j1tEuuS4iVT3Lrv8t6eVMRnlv4a2sAjdw7zDSxLEYB04K6R5SMk+vzqUcE4RFaxCGIHSCxydzliScnv1x9K4V98ReHxbePrOM4jUt9M/dz8zUb4h8TZpD/odsSMYzIGLFj0ChMg46/IdKd0WHxbiUUEZkmcKg6k53ztj3PtUP4H4fDbJ7iJmuIZZg4AGkqjsFGlSMsQMbbZ7VFhwGeY/aJneRlI1TXOqKKPBOcI3nbGehAGR77cfj3G7eOVTBNPdSRsGEkpAiBHTTGANgQDtgfOrIJ5xH4kNDdyWz2jOFI0lG8zAjOdJH5A+td3l7nW1u2McblZFBJRxpOBjf071U9hz5dyyAOIJJWZBFJIijwW+7kYGw82/XvVl3HL1nxGJWcpJKnkeaHbzjBYe+/Y560sxNTCgNUI45LccMtVaCRZY0J1/aGYuSzDAUrtt6V78A5/tbho4mYpKyjIZSBrIGVBPfPTNZxUxBpZrBqA1QelKsSaMfKgz1e9FY59qKADUE0s+lGagY9KM0jS1VQ6DSNaPEuLQQY8aVI9Wy62Azjr1NBuPIFBJIAHUn+tVtzP8R8pcRWccrFBpNwoBVTnBPQ+hwTj1qMc284m7eeP7QyQLkRxou03958/d2zUfi5quVQLE4iVGYgRgAZYAYIH3hsfvZ61ucU1IuO8cNlCIY2d72eKN5rh2JKBhq0Jndeufr+ECgchtWdwc5O5znIP41lxe+kmmaaU5d8En6AdvbFayN0rpJ0zvbvXPFiw1yrqcIEjcHB1hlYuW6sQu2/bHpXmnMRc5mtbeYnI1FNDEk5yWjK5PYE71yE3Ne8cW/qewp6Nda5Uu6Qx2q6zuBEzvrDqGABJ/dwc49/SpJZ8l3cqqUtIVPTULj0OCTpdh1yNvQ1FbTij25Zo5NLsCuf9xgc4J3B36irH5PjnWHFjCIRpHi3Nzn54SPUdKjLH0PX3rFahcJ5FVUZr2COIqMCV7lip264BHQdBnt0HfjG04VG5EIur2Qf/AAoCgOcnGV3/ADBzW3x7i1jC6tLK/FJtXmVn/ZIMHOFUaSfY5+lbfCOcrq7kEFpbraRb6pFTX4aAHc7BR29/woI7w7jVpBcPJJbh00jTb6V8rZByxIUahg+UDAyakHDPiFeTygwWqsq5/YxLnU24BdyPIMb7DtUXsbKwuLuWae5fwV87FwA8zliWChBsD6fe3/CQfEDm+KOJbOxCohVG8SI4GnGQq6fzzQRrn7jtzPOyzv5VO0SOGRdumV2Y9iT3z0qKE/oUtWf86xJ3rcmM2ti1lCsGKhsHpnGR6ZG9S6154McTCIGORgUVRtDHGepCg5Mmd9bepqI2lszsFUEk4ACgkkn0A6mpTzZy3DZW8ALN9rlCu6MRhF0nI2GB5vXJ2NS4s1rcA4zbIsgukknOxjQufD1d9QznJ9d9qsaJLHi0bGOPwLiNI2MgXTobBwuofeA0/wAsVTMEeBnv2FTLk2XySwh2UPgzOCdKQINTYHqxOM1nlCJXybz2UdLS8O3mCXLEgNhiBkt27avlmrOVgRkHIqmOKWi3CCMssasDLHqH+rW6gKowO77HTnfY9al/wk4m8tq0Tkt4L6FYgbrgED3x79iKzY0nXSmKxFArAef1+hRRj2/OigxNOlR9aoY/WKMViRRQDnbOdqoX4scbjuLpDEweMRgAjPXU2dj9OlXBzjxHwLOeXuEIG+N28o/nXzZcnUTjfA9f863wnaVjGQazR8An26flXggPYV6O23vXVlkqgq2o4IAK7dfXft6++K8EqT8f4ZbQ2tpLG7vJOpZgwAUDcbY3BDDHvioxGxz70iV72yMzAKCWPQCtmY6MjOrtkHbbbbPUda12yqgno2QD0z2I+W9ZuyeGFA825Ykj12C+nqT39sVFbNheCN1MYV5CGDPINSqW2BUeq9dW+/Qeu1cytl3e4kfWfMfMpfGxLDPTfG/4Vw0ON+uKnPKkVpDGLi4H2iYjKQqQVjC480pPlU5x5TnA3x6TksZ8B5KZo/tN4wtrUYJzszLjIwMbZ6b7nJwK9eJ81eIhsOF27JCww5VSZH7MTjJx0yx39cV1hxCfirrG+JLYSAypGdEaID+9K3mZwN8KAPetjm7ilnFayWlgoXOA0kZVR97JXP3pM7ggevWs/wBVWcFoSuubVHCCV1KuSz4+6D09z6Dsa53U4ru8Z4VciJWOTCmAPMAgJ7qM4OTncdetceAhfnnJx1x863GRIwboMbDYkdQNyOn4fzrKOId8f+KcxGASBnHUY6bnf1rwEpAxQSHk6ESXsCH7pfzEMQcKCxOQdjgV2viJx+zvpBJE8iui6MsnlcAkrg5yDkkbj8KPhG0bXpUgazE/hk5yG2yR2Bxnr/nXnf8AD1k4a2vT9ot7mZWYqSzKF1FdSg7ZbO+3vWfrSHu/ofx/pWzY3fgEZIZX0l1B6qGzpO/fFaNJ/wDv+JrWMp9wXjYnBd1Ls0imSLtI/wB2GNQOkYAyalnJuuz4jPZOAFmAlTT0BxkgZ9sj/wCtVXyjxNre5R1VXbdY9RwFd/KG+mTVnc5MYLjhc7P4jBgjuOjAlMtkbdya58p21FlU81gKZNc1ZfSnWGkelFBlSb/OiiqMJO3zFei96dFREU+Jn+z5vnH/AIxXzr6/P+tKiu3+aV69z9P6UpfvUUVplO+Jf7FtvlJ/zlqv4etFFOP1b8e11+5/dP8AjasJO3yX+VFFVAnQ/KujY/6tJ/8Atj/kaKKUntvcuf2F5/cT/Ea2LT+0g/uD/DSorHJuO/8AGH/2f7ifyaq3H9P86KK1x9M8vbbm/e/XcVqnrSoqwqW/Cv8A2pb/ACf/AJTVM4v7W6//ALJf+TRRWOXtZ6VLH1P0rH96iitRGUff5f1qxOO/7P4R8x/SiipyWLn/AO1N6KK4NCiiiiv/2Q==\n";
    var image = new Parse.File('image.png', {base64: base64});

    var user = new Parse.User();
    user.set('firstName', firstName);
    user.set('lastName', lastName);
    user.set('username', email);
    user.set('email', email);
    user.set('password', password);
    user.set('image', image);
    user.set('phone', phone);

    user.signUp(null, {
        useMasterKey: true,
        success: function(result) {
            response.success({"code":0,"message":"new user has been registered"});
        },
        error: function(user, error) {
            response.error("ERR-SAVE-USER");
        }
    });

});

Parse.Cloud.define('sign_in', function (request, response) {
    var username = request.params.username;
    var password = request.params.password;
    Parse.User.logIn(username, password).then(function (user) {
        response.success(user);
    }).catch(function (error) {
       response.error(error);
    });

});
// update user

Parse.Cloud.define('update_user', function (request, response) {
    var userId = request.params.userId;
    var firstName = request.params.firstName;
    var lastName = request.params.lastName;
    var email = request.params.email;
    var password = request.params.password;
    var phone = request.params.phone;

    var user = new Parse.User();
    user.id = userId;

    user.set('firstName', firstName);
    user.set('lastName', lastName);
    user.set('email', email);
    user.set('username', email);
    user.set('password', password);
    user.set('phone', phone);

    user.save(null, {
        useMasterKey: true,
        success: function(result) {
            response.success({"code":0,"message":"new user has been save"});
        },
        error: function(user, error) {
            response.error("ERR-UPDATE-USER");
        }
    });

})


//=============================
//            COFFEE
//=============================
Parse.Cloud.define('list_cafe', function (request, response) {

       var coffee = new Parse.Query('Cafe');
       coffee.include('category');
       coffee.find({
           success: function (coffee) {
               response.success(coffee)
           },
           error: function (error) {
               response.error("ERR-GET-CAFE!")
           }
       });
});

// Category Detail


Parse.Cloud.define('cafe_category', function (request, response) {
    var categoryTitle = request.params.categoryTitle;
    var cafe = new Parse.Query('Cafe');

    var cafeCategory = new Parse.Query('CafeCategory');
    cafeCategory.equalTo("title", categoryTitle);
    cafe.matchesQuery("category", cafeCategory);
    cafe.find({
        success: function (cafe) {
            response.success(cafe)
        },
        error: function (error) {
            response.error("ERR-GET-CAFE_CATEGORY!")
        }
    });
});

// Save Order Cafe

Parse.Cloud.define('order_cafe', function (request, response) {
    var cafeId = request.params.cafeId;
    var userId = request.params.userId;
    var price = request.params.price;
    var qty = request.params.qty;

    var OrderCafe = Parse.Object.extend('Cafe');
    var orderCafe = new OrderCafe();
    orderCafe.id = cafeId;

    var user = new Parse.User();
    user.id = userId;

    var cafeOrderQuery = new Parse.Query('OrderCafe');
    cafeOrderQuery.equalTo('user', user);
    cafeOrderQuery.equalTo('cafe', orderCafe);

    cafeOrderQuery.find().then(function (c) {
        if (c.length>0) {

            var Cafe = Parse.Object.extend('OrderCafe');
            var cafe = new Cafe();
            cafe.id = c[0].id;
            cafe.set('qty', c[0].get("qty")+1);

            cafe.set('price',price*(c[0].get("qty")+1))

            cafe.save(null, {
                useMasterKey: true,
                success: function(result) {
                    response.success({"code":0,"message":"new order cafe has been registered"});
                },
                error: function(error) {
                    response.error("ERR-SAVE-ORDER");
                }
            });
        }else {
            var Cafe = Parse.Object.extend('OrderCafe');
            var cafe = new Cafe();
            cafe.set('price', price);
            cafe.set('qty', qty);
            cafe.set('user', user);
            cafe.set('cafe', orderCafe);

            cafe.save(null, {
                useMasterKey: true,
                success: function(result) {
                    response.success({"code":0,"message":"new order cafe has been registered"});
                },
                error: function(orderCafe, error) {
                    response.error("ERR-SAVE-ORDER");
                }
            });
        }
    })



});
// save order success

Parse.Cloud.define('save_order_success', function (request, response) {

    var cafeId = request.params.cafeId;
    var userId = request.params.userId;

    var user = new Parse.User();
    user.id = userId;

    for(let i=0; i<cafeId.length; i++){
        var SaveOrder = Parse.Object.extend('SaveOrder');
        var saveOrder = new SaveOrder();

        var Cafe = Parse.Object.extend('Cafe');
        var cafe = new Cafe();
        cafe.id = cafeId[i];

        saveOrder.set('user', user);
        saveOrder.set('cafe', cafe);

        saveOrder.save(null, {
            useMasterKey: true,
            success: function(result) {
                response.success({"code":0,"message":"order cafe success save"});
            },
            error: function(orderCafe, error) {
                response.error("ERR-SAVE-ORDER");
            }
        });
    }
});

// Clear order when user order success

Parse.Cloud.define('clear_order_success', function (request, response) {

    var saveOrderId = request.params.saveOrderId;

    for (let i=0; i<saveOrderId.length; i++) {

        var SaveOrder = Parse.Object.extend('OrderCafe');
        var saveOrder = new SaveOrder();

        saveOrder.id = saveOrderId[i];

        saveOrder.destroy({
            success: function(result) {
                response.success({"code":0,"message":"delete order"});
            },
            error: function(orderCafe, error) {
                response.error("ERR-DELETE-ORDER");
            }

        })
    }

});
// delete order when user order wrong


Parse.Cloud.define('delete_order', function (request, response) {

    var saveOrderId = request.params.saveOrderId;

        var SaveOrder = Parse.Object.extend('OrderCafe');
        var saveOrder = new SaveOrder();

        saveOrder.id = saveOrderId;

        saveOrder.destroy({
            success: function(result) {
                response.success({"code":0,"message":"delete order"});
            },
            error: function(orderCafe, error) {
                response.error("ERR-DELETE-ORDER");
            }
        })
});


// Query Order Cafe

Parse.Cloud.define('get_order_cafe', function (request, response) {

    var userId = request.params.userId;

    var user = new Parse.User();
    user.id = userId;

    var cafeOrder = new Parse.Query('OrderCafe');
    cafeOrder.include('cafe');
    cafeOrder.equalTo("user", user);
    cafeOrder.find({
        success: function (cafeOder) {
            response.success(cafeOder)
        },
        error: function (error) {
            response.error("ERR-GET-ORDER-CAFE!")
        }
    });
});





















//////////////////////////////
/// working with phone number
/////////////////////////////
Parse.Cloud.define('sendCode', function(request, response) {  
    Parse.Cloud.useMasterKey();
    
    var phoneNumber = request.params.phoneNumber; 
    
    if (phoneNumber) {
        
        // Twilio Credentials 
        var accountSid = 'AC5b05f35a3977aa44314ac411977960e5'; 
        var authToken = '5df9ca8e4e82d90429636be5c060e910'; 

        //require the Twilio module and create a REST client 
        var client = require('twilio')(accountSid, authToken); 
        var code = randomInt(1111,9999)+"";
        var message = "iOne Verify code: " + code;
        
        
        
        var query = new Parse.Query(Parse.User);
        query.equalTo("phone", phoneNumber);
        query.first({
          useMasterKey: true,
          success: function(user) {
              
            // Handle user
            if (user !== null && typeof user === 'object') {
                // existing user
                user.set("password", code);
                user.save(null, {
                    useMasterKey: true,
                    success: function(user) {
                            // send sms code to phone number
                          client.messages.create({ 
                                to: phoneNumber, 
                                from: "iOne Card", 
                                body: message,  
                            }, function(err, message) { 
                                if (err) {
                                    response.error("ERR-AUTH-SMSERR");
                                }
                                else {
                                    response.success({"code": code});
                                }
                            });
                      },
                      error: function(user, error) {
                            response.error(error);
                      }
                    });
            }
            else {
                // new user
                var user = new Parse.User();
                user.set("username", phoneNumber);
                user.set("password", code);
                user.set("phone", phoneNumber);

                user.signUp(null, {
                  success: function(user) {

                        // send sms code to phone number
                      client.messages.create({ 
                            to: phoneNumber, 
                            from: "iOne Card", 
                            body: code,  
                        }, function(err, message) { 
                            if (err) {
                                response.error("ERR-AUTH-SMSERR");
                            }
                            else {
                                response.success({"code": code});
                            }
                        });
                  },
                  error: function(user, error) {
                      response.error("ERR-AUTH-NOCARD");
                  }
                });
            }
              
    
              
          },
          error: function(error) {
                response.error("ERR-AUTH-NOPHONE");
          }
        });

        
        

        
    }
    else {
        response.error("ERR-AUTH-NOPHONE");
    }
    
});




function randomInt(low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}



Parse.Cloud.define('loginWithPhone', function(request, response) {  
    var phoneNumber = request.params.phoneNumber; 
    var code = request.params.code;
    
    if (phoneNumber && code) {
        var query = new Parse.Query(Parse.User);
        query.equalTo("phone", phoneNumber);
        query.equalTo("opt", code);
        query.first({
            success: function(resultUser) {
                if (resultUser) {
                    // login
                    Parse.User.logIn(resultUser.get('username'), code, {
                      success: function(user) {
                            // Do stuff after successful login.
                          response.success(user);
                      },
                      error: function(user, error) {
                          // The login failed. Check error to see why.
                          response.error("ERR-AUTH-LOGINWITHPHONE");
                      }
                    });
                }
                else {
                    response.error("ERR-AUTH-NOUSER");
                }
                
            },
            error: function(error) {
                response.error("ERR-AUTH-LOGINWITHPHONE");
            }
        });

    }
    else {
        response.error("ERR-AUTH-NOPHONE");
    }
    
});


Parse.Cloud.define('activateCardTest', function(request, response) {
var cardNumber = request.params.cardNumber;
	var queryEqual = new Parse.Query("Card");
	 queryEqual.equalTo("cardNumber", cardNumber);
queryEqual.find().then(function(results) {
    // results contains a weight / rank in result.get('score')
	response.success(results);  
})
  .catch(function(error) {
    // There was an error.
response.success(error);
  });

});


Parse.Cloud.define('activateCard', function(request, response) {
    Parse.Cloud.useMasterKey();
    var cardNumber = request.params.cardNumber; 
    
    var queryStart = new Parse.Query("Card");
    queryStart.endsWith("cardNumber", cardNumber);
    
    var queryEnd = new Parse.Query("Card");
    queryEnd.startsWith("cardNumber", cardNumber);
    
    var queryEqual = new Parse.Query("Card");
    queryEqual.equalTo("cardNumber", cardNumber);
    
    var query = Parse.Query.or(queryEqual);
    queryEqual.include("user");
    queryEqual.first({
        success: function(card) {
            if (card && card.get("user")) {                
                var phoneNumber = card.get("user").get("phone"); 
                
                if (phoneNumber) {
        
                    if (phoneNumber.indexOf("+") == -1) {
                        phoneNumber = "+" + phoneNumber;
                    }
                    
                    // Twilio Credentials 
                    var accountSid = 'AC5b05f35a3977aa44314ac411977960e5'; 
                    var authToken = '5df9ca8e4e82d90429636be5c060e910'; 

                    //require the Twilio module and create a REST client 
                    var client = require('twilio')(accountSid, authToken); 
                    var code = randomInt(1111,9999)+"";
                    var message = "iOne Verify code: " + code;



                    var query = new Parse.Query(Parse.User);
                    query.equalTo("objectId", card.get("user").id);
                    query.first({
                      useMasterKey: true,
                      success: function(user) {

                        // Handle user
                        if (user !== null && typeof user === 'object') {
                            // existing user
                            user.setPassword(code+"");
                            user.set("opt", code);
                            user.save(null, {
                                useMasterKey: true,
                                success: function(user) {
                                        // send sms code to phone number
                                      client.messages.create({ 
                                            to: phoneNumber, 
                                            from: "iOne Card", 
                                            body: message,  
                                        }, function(err, message) { 
                                            if (err) {
                                                response.error("ERR-AUTH-SMSERR");
                                            }
                                            else {
                                                response.success({"phoneNumber":phoneNumber});
                                            }
                                        });
                                  },
                                  error: function(user, error) {
                                        response.error("ERR-AUTH-OPT");
                                  }
                                });
                        }
                        else {
                            response.error("ERR-AUTH-NOUSER");
                        }



                      },
                      error: function(error) {

                          response.error("ERR-AUTH-NOPHONE");
                      }
                    });

                }
                else {
                    response.error("ERR-AUTH-NOPHONE");
                }
                
                
                
            }
            else {
                if (card) {
                    response.error("ERR-AUTH-NOPHONE");
                }
                else {
                    response.error("ERR-AUTH-NOCARD"); 
                }
                
            }
            
        },
        error: function(error) {
            response.error("ERR-AUTH-NOCARD");
        }
    });
});




function randomInt(low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}



///////////////////////////////////
/// Working with push notification
///////////////////////////////////





/////////////////////////////////
// Get available preorder product
//////////////////////////////////
Parse.Cloud.define('preorder', function(request, response) {
    var query = new Parse.Query("PreOrder");
    query.equalTo("status", true);
    query.first({
        success: function(preorder) {
            response.success(preorder);
        },
        error: function(error) {
            response.error("ERR-PREORDER-NOTFOUND");
        }
    });
});

Parse.Cloud.define('preorderdetail', function(request, response) {
    var preorderId = request.params.preorderId; 
    var PreOrder = Parse.Object.extend("PreOrder");
    var preorder =  new PreOrder();
    preorder.id = preorderId;
    
    var arrDetail = [];


    var query = new Parse.Query("PreOrderDetail");
    query.equalTo("preorder", preorder);
    query.find().then(function(details){
      var promises = [];
      _.each(details, function(detail){

            var dictDetail = {};
            dictDetail['productName'] = detail.get('productName');
            dictDetail['objectId'] = detail.id;
            dictDetail['spec'] = detail.get('spec');


            var detail = {"__type":"Pointer","className":"PreOrderDetail", "objectId":detail.id};


            // get product appearence for each detail
            var appearent_query = new Parse.Query("ProductAppearence");
            appearent_query.equalTo("detail", detail);
            var promise1 = appearent_query.find().then(function(appearents){

                var arrAppearances = [];
                _.each(appearents, function(appearent) {
                    var dict = {};
                    dict['photo'] = appearent.get('photo').url();
                    dict['title'] = appearent.get('title');
                    dict['prices'] = appearent.get('prices');
                    dict['objectId'] = appearent.id;
                    arrAppearances.push(dict);
                });
                dictDetail['appearance'] = arrAppearances;

            });


            // get product price for each detail
            var price_query = new Parse.Query("ProductPrice");
            price_query.equalTo("detail", detail);
            var promise2 = price_query.find().then(function(prices){

                var arrPrices = [];
                _.each(prices, function(price) {
                    var dict = {};
                    dict['price'] = price.get('price');
                    dict['capacity'] = price.get('capacity');
                    dict['objectId'] = price.id;
                    arrPrices.push(dict);
                });
                dictDetail['prices'] = arrPrices;

            });

             promises.push(promise1);
             promises.push(promise2);
             arrDetail.push(dictDetail);


      });

      //if returning another promise, the ".then" won't execute until it completes.
      return Parse.Promise.when(promises); 
    }).then(function(){   
      response.success(arrDetail);
    });
            
            
      
});





/// User preorder product
Parse.Cloud.define('submitpreorder', function(request, response) {
    var userId = request.params.userId;
    var detailId = request.params.detailId;
    var preorderId = request.params.preorderId;
    var priceId = request.params.priceId;
    var appearentId = request.params.appearentId;
    
    if (userId && detailId && preorderId && priceId && appearentId) {
        
        var user = {"__type":"Pointer","className":"_User", "objectId":userId};
        var detail = {"__type":"Pointer","className":"PreOrderDetail", "objectId":detailId};
        var preorder = {"__type":"Pointer","className":"PreOrder", "objectId":preorderId};
        var appearence = {"__type":"Pointer","className":"ProductAppearence", "objectId":appearentId};
        var price = {"__type":"Pointer","className":"ProductPrice", "objectId":priceId};
        
        var UserPreorder = Parse.Object.extend("UserPreorder");
        var userPreorder = new UserPreorder();
        userPreorder.set('user', user);
        userPreorder.set('preorder', preorder);
        userPreorder.set('detail', detail);
        userPreorder.set('appearence', appearence);
        userPreorder.set('price', price);
        var code = Math.floor(Math.random() * 999999) + 100000;
        userPreorder.set('code', code.toString());
        userPreorder.set('orderdate', new Date());
        userPreorder.save({
          success: function(result) {
              response.success(result);
          },
          error: function(error) {
              response.error("ERR-PREORDER-SAVE");
          }
        });
        
    }
    else {
        response.error("ERR-MISSING-PARAM");
    }
});


Parse.Cloud.define('getpreorder', function(request, response) {
    var userPreorderId = request.params.userPreorderId;
    var code = request.params.code;
    if (userPreorderId && code) {
       var query = new Parse.Query("UserPreorder");
        query.include("preorder");
        query.equalTo("objectId", userPreorderId);
        query.equalTo("objectId", userPreorderId);
        query.first({
            success: function(preorder) {
                if (preorder.get("preorder")) {
                     var redeemCode = preorder.get("preorder").get("redeemCode"); 

                    if (redeemCode == code) {
                        if (preorder.get("getProduct") == true) {
                            response.error("Product already get");
                        }
                        else {
                            preorder.set("getProduct", true);
                            preorder.save();
                            response.success("Get Product success");
                        }
                    }
                    else {
                        response.error("ERR-PREORDER-REDEEMCODE");
                    }
                }
                else {
                    response.error("ERR-PREORDER-NOTEXIST");
                }
               
               
            },
            error: function(error) {
                response.error("ERR-PREORDER-NOTEXIST");
            }
        }); 
    }
    else {
        response.error("ERR-MISSING-PARAM");
    }
    
});

////////////////////////
///// Shop Review
//////////////////////

Parse.Cloud.define('shopreview', function(request, response) {
    var userId = request.params.userId;
    var shopId = request.params.shopId;
    var star = request.params.star;
    var comment = request.params.comment;
    
    if (userId && shopId && star) {
        var user = {"__type":"Pointer","className":"_User", "objectId":userId};
        
        var query = new Parse.Query("Shop");
        query.get(shopId, {
          success: function(shop) {
              
                // find average star
                var totalStar = shop.get("star") ? shop.get("star") : 0;
                var divide = 2;
                if (totalStar == 0) {
                    divide = 1;
                }
                var averageStar = (totalStar+star)/divide;
              
              
                // add review
                var ShopReview = Parse.Object.extend("ShopReview");
                var shopReview = new ShopReview();
                shopReview.set('user', user);
                shopReview.set('shop', shop);
                shopReview.set('star', star);
                shopReview.set('comment', comment);
                shopReview.save({
                  success: function(result) {
                      
                        // update star to shop
                        shop.set('star', averageStar);
                        shop.save({
                            success: function(shopResult) {
                                response.success({ "averageStar" : shopResult.get("star") });
                            },
                            error: function(error) {
                                response.error("ERR-REVIEW-SAVE");
                            }
                        });
                        
                  },
                  error: function(error) {
                      response.error("ERR-REVIEW-SAVE");
                  }
                });
                

                
          },
          error: function(error) {
              response.error("ERR-REVIEW-NOSHOP");
          }
        });
        
        
    }
    else {
        response.error("ERR-MISSING-PARAM");
    }
});



////////////////////////
///// Shop
//////////////////////


Parse.Cloud.define('shopcategories', function(request, response) {
    var userId = request.params.userId;
    if (userId) {
        var query = new Parse.Query("ShopCategory");
        query.find({
          success: function(categories) {
               
                var user = {"__type":"Pointer","className":"_User", "objectId":userId};
                var query = new Parse.Query("CategoryFollow");
                query.equalTo("user", user);
                query.find({
                    success: function(follows) {
                        
                        var categoryList = [];
                        for (var i = 0; i < categories.length; i++) {
                            var category = categories[i];
                            var dict = {};
                            dict['name'] = category.get('name');
                            if (category.get('photo')) {
                                dict['photo'] = category.get('photo').url();
                            }
                            dict['objectId'] = category.id;
                            dict['followed'] = false;
                            for (var j = 0; j < follows.length; j++) {
                                var follow = follows[j];
                                if (category.id == follow.get("category").id) {
                                    dict['followed'] = true;
                                }
                            }
                            categoryList.push(dict);
                        }
                        response.success(categoryList);
                    

                    },
                    error: function(error) {
                        response.error("ERR-FOLLOW-NOTFOUND");
                    }
                });
          },
          error: function(error) {
              response.error("ERR-SHOPCAT-NOTFOUND");
          }
        });
    }
    else {
        response.error("ERR-MISSING-PARAM");
    }    
        
});




Parse.Cloud.define('shopcategories_guest', function(request, response) {
   
        var query = new Parse.Query("ShopCategory");
        query.find({
          success: function(categories) {
               
              response.success(categories);
        
          },
          error: function(error) {
              response.error("ERR-SHOPCAT-NOTFOUND");
          }
        });
      
        
});



Parse.Cloud.define('followshop', function(request, response) {
     Parse.Cloud.useMasterKey();
    
    var userId = request.params.userId;
    var shopId = request.params.shopId;
    var follow = request.params.follow;
    if (userId && shopId) {
        
        var user = {"__type":"Pointer","className":"_User", "objectId":userId};
        var shop = {"__type":"Pointer","className":"Shop", "objectId":shopId};
        
        var query = new Parse.Query("ShopFollow");
        query.equalTo("user", user);
        query.equalTo("shop", shop);
        query.first({
          success: function(currentFollow) {
               
              
               if (currentFollow) { // User already follow
                   
                   if (follow == 0) {
                       // Unfollow shop
                       currentFollow.destroy({
                            success: function(result) {
                                response.success("Unfollow success");
                            },
                            error: function(error) {
                                response.error("ERR-UNFOLLOW");
                            }
                        });
                   }
                   else {
                        response.error("ERR-FOLLOW-ALREADY"); 
                   }
               }
                else { // User didn't follow yet
                    
                    if (follow == 1) { 
                        // follow shop
                        var ShopFollow = Parse.Object.extend("ShopFollow");
                        var followShop = new ShopFollow();
                        followShop.set('user', user);
                        followShop.set('shop', shop);
                        followShop.save();

                        response.success("Follow success");
                        
                    }
                    else { // error 
                        response.error("ERR-UNFOLLOW-NOTYET");  
                    }
                    
                }
              
              
          },
          error: function(error) {
              response.error("ERR-FOLLOW-SHOPQUERY");
          }
        });
    }
    else {
        response.error("ERR-MISSING-PARAM");
    }    
        
});



Parse.Cloud.define('followcategory', function(request, response) {
     Parse.Cloud.useMasterKey();
    
    var userId = request.params.userId;
    var catId = request.params.catId;
    var follow = request.params.follow;
    if (userId && catId) {
        
        var user = {"__type":"Pointer","className":"_User", "objectId":userId};
        var category = {"__type":"Pointer","className":"ShopCategory", "objectId":catId};
        
        var query = new Parse.Query("CategoryFollow");
        query.equalTo("user", user);
        query.equalTo("category", category);
        query.first({
          success: function(currentFollow) {
               
              
               if (currentFollow) { // User already follow
                   
                   if (follow == 0) {
                       // Unfollow shop
                       currentFollow.destroy({
                            success: function(result) {
                                response.success("Unfollow success");
                            },
                            error: function(error) {
                                response.error("ERR-UNFOLLOW");
                            }
                        });
                   }
                   else {
                        response.error("ERR-FOLLOW-ALREADY"); 
                   }
               }
                else { // User didn't follow yet
                    
                    if (follow == 1) { 
                        // follow shop
                        var ShopFollow = Parse.Object.extend("CategoryFollow");
                        var followShop = new ShopFollow();
                        followShop.set('user', user);
                        followShop.set('category', category);
                        followShop.save();

                        response.success("Follow success");
                        
                    }
                    else { // error 
                        response.error("ERR-UNFOLLOW-NOTYET");  
                    }
                    
                }
              
              
          },
          error: function(error) {
              response.error('ERR-FOLLOW-CATQUERY');
          }
        });
    }
    else {
        response.error('ERR-MISSING-PARAM');
    }    
        
});



////////////////////////
///// Offer
//////////////////////


Parse.Cloud.define('claimoffer-bak', function(request, response) {
     Parse.Cloud.useMasterKey();
    
    var userId = request.params.userId;
    var offerId = request.params.offerId;
    if (userId && offerId) {
        
        var user = {"__type":"Pointer","className":"_User", "objectId":userId};
        
        var queryOffer = new Parse.Query("Offer");
        queryOffer.equalTo("objectId", offerId);
        queryOffer.first({
            success: function(offer) {
                
                if (offer) {
                    
                    var query = new Parse.Query("UserCoupon");
                    query.equalTo("user", user);
                    query.equalTo("coupon", offer);
                    query.count({
                      success: function(currentCoupon) {
                          
                          
                          if (currentCoupon) { // user already have offer
                              response.error("ERR-CLAIM-READY");
                          } 
                          else { // user haven't claim offer yet
                              // add user
                                var UserCoupon = Parse.Object.extend("UserCoupon");
                                var userCoupon = new UserCoupon();
                                userCoupon.set('user', user);
                                userCoupon.set('coupon', offer);
                                userCoupon.set('validAmount', offer.get('amount'));
                                userCoupon.save();

                                response.success("Claim offer success");
                          }


                      },
                      error: function(error) {
                          response.error('ERR-CLAIM-QUERY');
                      }
                    });
                    
                    
                }
                else {
                    response.error('ERR-CLAIM-INVOFFER');  
                }
            },
            error: function(object, error) {
                response.error('ERR-CLAIM-INVOFFER');
            }
        });
       
    }
    else {
        response.error('ERR-MISSING-PARAM');
    }    
        
});



Parse.Cloud.define('claimoffer', function(request, response) {
     Parse.Cloud.useMasterKey();
    
    var userId = request.params.userId;
    var offerId = request.params.offerId;
    if (userId && offerId) {
        
        var user = {"__type":"Pointer","className":"_User", "objectId":userId};
        
        var queryOffer = new Parse.Query("Offer");
        queryOffer.equalTo("objectId", offerId);
        queryOffer.first({
            success: function(offer) {
                
                if (offer) {
                    
                    var query = new Parse.Query("UserCoupon");
                    query.equalTo("user", user);
                    query.equalTo("coupon", offer);
                    query.count({
                      success: function(currentCoupon) {
                          
                          
                          if (currentCoupon) { // user already have offer
                              response.error("ERR-CLAIM-READY");
                          } 
                          else { // user haven't claim offer yet
                              // add user
                                var UserCoupon = Parse.Object.extend("UserCoupon");
                                var userCoupon = new UserCoupon();
                                userCoupon.set('user', user);
                                userCoupon.set('coupon', offer);
                                userCoupon.set('validAmount', offer.get('amount'));
                                userCoupon.save();
                              
                              userCoupon.save(null, {
                                useMasterKey: true,
                                success: function(result) {
                                        response.success(result);
                                  },
                                  error: function(user, error) {
                                        response.error("ERR-USERCOUPON-SAVE");
                                  }
                                });

                                
                          }


                      },
                      error: function(error) {
                          response.error('ERR-CLAIM-QUERY');
                      }
                    });
                    
                    
                }
                else {
                    response.error('ERR-CLAIM-INVOFFER');  
                }
            },
            error: function(object, error) {
                response.error('ERR-CLAIM-INVOFFER');
            }
        });
       
    }
    else {
        response.error('ERR-MISSING-PARAM');
    }    
        
});


Parse.Cloud.define('redeemcoupon', function(request, response) {
    Parse.Cloud.useMasterKey();
    
    var userId = request.params.userId;
    var offerId = request.params.offerId;
    var shopCode = request.params.shopCode;
    if (userId && offerId && shopCode) {
        
        var user = {"__type":"Pointer","className":"_User", "objectId":userId};
        
        var queryOffer = new Parse.Query("Offer");
        queryOffer.equalTo("objectId", offerId);
        queryOffer.include("shop");
        queryOffer.first({
            success: function(offer) {
                
                if (offer) {
                    
                    if (offer.get("shop").get("code") == shopCode) { // compare shop code user input with server
                        
                        // reduce -1 for user valid coupon
                        var query = new Parse.Query("UserCoupon");
                        query.equalTo("user", user);
                        query.equalTo("coupon", offer);
                        query.first({
                          success: function(userCoupon) {
                              
                              if (userCoupon) { // user already have offer
                                    if (userCoupon.get('validAmount') <= 0) {
                                        response.error("ERR-REDEEM-NOVALIDAMNT");
                                    }
                                    else {
                                        
                                        if (offer.get("instock") > 0) {
                                            
                                            if (userCoupon.get('validAmount') > 0) {
                                                userCoupon.set('validAmount', userCoupon.get('validAmount') - 1);
                                                userCoupon.save();

                                                var UserRedeem = Parse.Object.extend("UserRedeem");
                                                var userRedeem = new UserRedeem();
                                                userRedeem.set('user', user);
                                                userRedeem.set('offer', offer);
                                                userRedeem.save();

                                                offer.set("instock", offer.get("instock") - 1)
                                                offer.save();
                                                
                                                response.success("Redeem coupon success");
                                            }
                                            else {
                                                response.error("ERR-REDEEM-NOVALIDAMNT");
                                            }
                                            
                                        }
                                        else {
                                            response.error("ERR-REDEEM-OUTOFSTOCK"); 
                                        }
                                        
                                    }
                                    
                              } 
                              else { 
                                   response.error("ERR-COUPON-NOTFOUND"); 
                              }


                          },
                          error: function(error) {
                              response.error('ERR-USERCOUPON-QUERY');
                          }
                        });

                    }
                    else {
                        response.error("ERR-REDEEM-INVLIDSHOPCODE");
                    }
                    
                    
                }
                else {
                    response.error('ERR-COUPON-NOAVAILABLE');  
                }
            },
            error: function(object, error) {
                response.error('ERR-REDEEM-QUERY');
            }
        });
       
    }
    else {
        response.error('ERR-MISSING-PARAM');
    }    
        
});




/*
***************************
***** Push Notification ****
***************************
*/

Parse.Cloud.define('pushnotification_dev', function(request, response) {
    var playerId = request.params.playerId;
    var message = request.params.message;
    
    if (playerId) {
          var sendNotification = function(data) {
          var headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Basic NDFhZjU3Y2MtYTBiMS00YzBmLWI4MjMtODBjNjIxMDE0OGRm"
          };

          var options = {
            host: "onesignal.com",
            port: 443,
            path: "/api/v1/notifications",
            method: "POST",
            headers: headers
          };

          var https = require('https');
          var req = https.request(options, function(res) {  
            res.on('data', function(data) {
              response.success(JSON.parse(data));
            });
          });

          req.on('error', function(e) {
            response.error(e);
          });

          req.write(JSON.stringify(data));
          req.end();
        };

        var playerIds = playerId.split(",");
        var message = { 
          app_id: "4f9bd22a-e631-46a5-b2e2-793bbea38d0f",
          contents: {"en": message},
          include_player_ids : playerIds
        };

        sendNotification(message);  
    }
    else {
        res.error("ERR-PUSH-PLAYID");
    }
});



Parse.Cloud.define('pushnotification', function(request, response) {
    var playerId = request.params.playerId;
    var message = request.params.message;
    
    if (playerId) {
          var sendNotification = function(data) {
          var headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Basic ZTFlMDYxZWQtODM5Ni00ODgzLTk2ZmQtZWM1ZWFjODcyMTYz"
          };

          var options = {
            host: "onesignal.com",
            port: 443,
            path: "/api/v1/notifications",
            method: "POST",
            headers: headers
          };

          var https = require('https');
          var req = https.request(options, function(res) {  
            res.on('data', function(data) {
              response.success(JSON.parse(data));
            });
          });

          req.on('error', function(e) {
            response.error(e);
          });

          req.write(JSON.stringify(data));
          req.end();
        };

        var playerIds = playerId.split(",");
        var message = { 
          app_id: "91800e24-b237-44f3-84aa-4f372f53adab",
          contents: {"en": message},
          include_player_ids : playerIds
        };

        sendNotification(message);  
    }
    else {
        res.error("ERR-PUSH-PLAYID");
    }
});




/*
****************************
***** Ecommerce feature ****
****************************
*/

Parse.Cloud.define('slideshow', function(request, response) {
    
    var query = new Parse.Query("SlideShow");
    query.equalTo("status", true);
    query.find({
        success: function(slideshows) {
            response.success(slideshows);
        },
        error: function(error) {
            response.error("ERR-SLIDESHOW-NOTFOUND");
        }
    });
    
});


Parse.Cloud.define('product_hot', function(request, response) {
    
    var arrResult = [];


    var query = new Parse.Query("Product");
    query.equalTo("status", true);
    query.equalTo("hot", true);
    query.descending("createdAt");
    query.find().then(function(products){
      var promises = [];
      _.each(products, function(product){

            var arrImage = [];
          
            // get product images
            var image_query = new Parse.Query("ProductImage");
            image_query.equalTo("product", product);
            image_query.select('image');
            var promise1 = image_query.find().then(function(images){
                    
                    
                    for(var i = 0; i < images.length;i++){
                        arrImage.push(images[i].get('image').url());
                    }
            
            });

             promises.push(promise1);
             arrResult.push(manipulateProductAndImages(product, arrImage));

      });

      //if returning another promise, the ".then" won't execute until it completes.
      return Parse.Promise.when(promises); 
    }).then(function(){   
      response.success(arrResult);
    });
          
    
});

// Parse.Cloud.define('product_hot_detail', function(request, response) {
//
//     var productId = request.params.productId;
//
//     if (productId) {
//         var arrResult = [];
//
//         var query = new Parse.Query("Product");
//         query.equalTo("status", true);
//         query.equalTo("hot", true);
//         query.equalTo("objectId", productId);
//         query.find().then(function(products){
//             var promises = [];
//             if (products.length > 0) {
//                 _.each(products, function(product){
//
//                     var arrImage = [];
//
//                     // get product images
//                     var image_query = new Parse.Query("ProductImage");
//                     image_query.equalTo("product", product);
//                     image_query.select('image');
//                     var promise1 = image_query.find().then(function(images){
//
//
//                         for(var i = 0; i < images.length;i++){
//                             arrImage.push(images[i].get('image').url());
//                         }
//
//                     });
//
//                     promises.push(promise1);
//                     arrResult.push(manipulateProductAndImages(product, arrImage));
//
//                 });
//             }
//
//
//             //if returning another promise, the ".then" won't execute until it completes.
//             return Parse.Promise.when(promises);
//         }).then(function(){
//             response.success(arrResult);
//         });
//     }
//     else {
//         response.error("ERR-MISSING-PARAM");
//     }
//
// });


Parse.Cloud.define('product_recommend', function(request, response) {
    
    var arrResult = [];


    var query = new Parse.Query("Product");
    query.equalTo("status", true);
    query.equalTo("recommend", true);
    query.descending("createdAt");
    query.find().then(function(products){
      var promises = [];
      _.each(products, function(product){

            var arrImage = [];
          
            // get product images
            var image_query = new Parse.Query("ProductImage");
            image_query.equalTo("product", product);
            image_query.select('image');
            var promise1 = image_query.find().then(function(images){
                    
                    
                    for(var i = 0; i < images.length;i++){
                        arrImage.push(images[i].get('image').url());
                    }
            
            });

             promises.push(promise1);
             arrResult.push(manipulateProductAndImages(product, arrImage));

      });

      //if returning another promise, the ".then" won't execute until it completes.
      return Parse.Promise.when(promises); 
    }).then(function(){   
      response.success(arrResult);
    });
          
    
});


Parse.Cloud.define('product_related', function(request, response) {
    
    var productId = request.params.productId;
    
    var arrResult = [];

    var query = new Parse.Query("Product");
    query.equalTo("status", true);
    query.equalTo("objectId", productId);
    query.find().then(function(products){
      var promises = [];
      _.each(products, function(product){

            var arrImage = [];
          
            // get product images
            var image_query = new Parse.Query("ProductImage");
            image_query.equalTo("product", product);
            image_query.select('image');
            var promise1 = image_query.find().then(function(images){
                    
                    
                    for(var i = 0; i < images.length;i++){
                        arrImage.push(images[i].get('image').url());
                    }
            
            });

             promises.push(promise1);
             arrResult.push(manipulateProductAndImages(product, arrImage));

      });

      //if returning another promise, the ".then" won't execute until it completes.
      return Parse.Promise.when(promises); 
    }).then(function(){   
      response.success(arrResult);
    });
          
    
});


Parse.Cloud.define('product_search', function(request, response) {
    
    var keyword = request.params.keyword;
    var sort = request.params.sort; // a-z, price, best match
    var filter = request.params.filter; // cate_id
    
    var arrResult = [];

    var query = new Parse.Query("Product");
    query.matches("name", keyword, 'i');
    if (filter) {
        var category = {"__type":"Pointer","className":"ProductCategory", "objectId":filter};
        query.equalTo('category', category);
    }
    if (sort) {
        if (sort == 'a-z') {
            query.ascending('name');
        }
        else if (sort == 'price') {
              query.ascending('price');   
        }
    }

    query.equalTo("status", true);
    query.find().then(function(products){
        var promises = [];
        if (products.length > 0) {
                    _.each(products, function(product){

                    var arrImage = [];

                    // get product images
                    var image_query = new Parse.Query("ProductImage");
                    image_query.equalTo("product", product);
                    image_query.select('image');
                    var promise1 = image_query.find().then(function(images){


                            for(var i = 0; i < images.length;i++){
                                arrImage.push(images[i].get('image').url());
                            }

                    });

                     promises.push(promise1);
                     arrResult.push(manipulateProductAndImages(product, arrImage));

              });
        }
        

        //if returning another promise, the ".then" won't execute until it completes.
        return Parse.Promise.when(promises); 
    }).then(function(){   
      response.success(arrResult);
    });
          
    
});

Parse.Cloud.define('products', function(request, response) {
    
    var arrResult = [];

    var query = new Parse.Query("Product");
    query.equalTo("status", true); 
    query.descending("createdAt");
    query.find().then(function(products){
      var promises = [];
        if (products.length>0) {
            _.each(products, function(product){

                var arrImage = [];
          
                // get product images
                var image_query = new Parse.Query("ProductImage");
                image_query.equalTo("product", product);
                image_query.select('image');
                var promise1 = image_query.find().then(function(images){


                        for(var i = 0; i < images.length;i++){
                            arrImage.push(images[i].get('image').url());
                        }

                });

                 promises.push(promise1);
                 arrResult.push(manipulateProductAndImages(product, arrImage));

          });
        }
      

      //if returning another promise, the ".then" won't execute until it completes.
      return Parse.Promise.when(promises); 
    }).then(function(){   
      response.success(arrResult);
    });
          
    
});


Parse.Cloud.define('product_detail', function(request, response) {
    
    var productId = request.params.productId;
    
    if (productId) {
        var arrResult = [];

        var query = new Parse.Query("Product");
        query.equalTo("status", true);
        query.equalTo("objectId", productId);
        query.find().then(function(products){
          var promises = [];
            if (products.length > 0) {
                _.each(products, function(product){

                        var arrImage = [];

                        // get product images
                        var image_query = new Parse.Query("ProductImage");
                        image_query.equalTo("product", product);
                        image_query.select('image');
                        var promise1 = image_query.find().then(function(images){


                                for(var i = 0; i < images.length;i++){
                                    arrImage.push(images[i].get('image').url());
                                }

                        });

                         promises.push(promise1);
                         arrResult.push(manipulateProductAndImages(product, arrImage));

                  });
            }
          

          //if returning another promise, the ".then" won't execute until it completes.
          return Parse.Promise.when(promises); 
        }).then(function(){   
          response.success(arrResult);
        });
    }
    else {
        response.error("ERR-MISSING-PARAM");
    }

});




Parse.Cloud.define('shop_popular', function(request, response) {
    
   var query = new Parse.Query("Shop");
    query.equalTo("popular", true);
    query.find({
        success: function(shops) {
            response.success(shops);
        },
        error: function(error) {
            response.error("ERR-SHOP-NOTFOUND"  + error.code + " " + error.message);
        }
    });
          
});


Parse.Cloud.define('shop_new', function(request, response) {
    
   var query = new Parse.Query("Shop");
    query.descending("createdAt");
    query.limit(5); 
    
    query.find({
        success: function(shops) {
            response.success(shops);
        },
        error: function(error) {
            response.error("ERR-SHOP-NOTFOUND"  + error.code + " " + error.message);
        }
    });
          
});


Parse.Cloud.define('shop_category_home', function(request, response) {
    
   var query = new Parse.Query("ShopCategory");
    query.equalTo("feature", true);
    query.find({
        success: function(categories) {
            response.success(categories);
        },
        error: function(error) {
            response.error("ERR-SHOP-NOTFOUND"  + error.code + " " + error.message);
        }
    });
          
    
});



Parse.Cloud.define('offer_home', function(request, response) {
    
   var query = new Parse.Query("Offer");
    query.descending("createdAt");
    query.greaterThan("expired", new Date());
    query.find({
        success: function(offers) {
            response.success(offers);
        },
        error: function(error) {
            response.error("ERR-OFFER-NOTFOUND " + error.code + " " + error.message );
        }
    });
          
    
});

//my own define
Parse.Cloud.define('offer_detail', function(request, response) {
    var offerId = request.params.offerId;
    var query = new Parse.Query("Offer");
    query.equalTo('objectId', offerId);
    query.find({
        success: function(offers) {
            response.success(offers);
        },
        error: function(error) {
            response.error("ERR-OFFER-NOTFOUND"  + error.code + " " + error.message);
        }
    });

});


Parse.Cloud.define('my_cart', function(request, response) {
    var userId = request.params.userId;
    
    var user = new Parse.User();
    user.id = userId;
    
    var arrResult = [];
    
   var query = new Parse.Query("UserCart");
    query.equalTo("user", user);
    query.greaterThan("qty", 0);
    query.include("product");
    query.find().then(function(carts){
      var promises = [];
        
        if (carts.length == 0) {
              return Parse.Promise.when(promises); 
        }
        
      _.each(carts, function(cart){
          
          
          
            var arrImage = [];
          
            // get product images
            var product = cart.get('product');
            if (product) {
                var image_query = new Parse.Query("ProductImage");
                image_query.equalTo("product", product);
                image_query.select('image');
                var promise1 = image_query.find().then(function(images){
                    
                    
                    for(var i = 0; i < images.length;i++){
                        arrImage.push(images[i].get('image').url());
                    }
            
                });

                promises.push(promise1);
                arrResult.push(manipulateCartProductAndImages(cart, product, arrImage));
            }
            

      });

      //if returning another promise, the ".then" won't execute until it completes.
      return Parse.Promise.when(promises); 
    }).then(function(){   
      response.success(arrResult);
    });
    
});


Parse.Cloud.define('add_cart', function(request, response) {
    var userId = request.params.userId;
    var productId = request.params.productId;
    var size = request.params.size;
    var color = request.params.color;
    var qty = request.params.qty;
    var price = request.params.price;
   
    var Product = Parse.Object.extend("Product");
    var product = new Product();
    product.id = productId;
    
    var user = new Parse.User();
    user.id = userId;
    
    var Cart = Parse.Object.extend("UserCart");
    var cart = new Cart();
    cart.set('user', user);
    cart.set('product', product);
    cart.set('color', color);
    cart.set('size', size);
    cart.set('qty', qty);
    cart.set('price', price);
  
    cart.save(null, {
                    useMasterKey: true,
                    success: function(result) {
                           response.success({"code":0,"message":"you have added to cart"});
                      },
                      error: function(user, error) {
                            response.error("ERR-CART-SAVE");
                      }
                    });
});


Parse.Cloud.define('update_cart', function(request, response) {
    var cartId = request.params.cartId;
    var size = request.params.size;
    var color = request.params.color;
    var qty = request.params.qty;
    var price = request.params.price;
    
    
    var Cart = Parse.Object.extend("UserCart");
    var cart = new Cart();
    if (cartId) {
        cart.id = cartId;
    }
    else {
        cart.id = request.params.userId;
    }
    
    if (color) {
        cart.set('color', color);
    }
    if (size) {
        cart.set('size', size);  
    }
    cart.set('qty', qty); 
    
    if (price) {
       cart.set('price', price); 
    }
    
  
    cart.save(null, {
                    useMasterKey: true,
                    success: function(result) {
                           response.success({"code":0,"message":"you have added to cart"});
                      },
                      error: function(user, error) {
                            response.error("ERR-CART-SAVE");
                      }
                    });
});


Parse.Cloud.define('pre_auth', function(request, response) {
    
    var userId = request.params.userId;
    var uuid = request.params.uuid;
    var total = request.params.total;
    var paymethod = request.params.paymethod;
    
    var user = new Parse.User();
    user.id = userId;
    
    var query = new Parse.Query("UserCart");
    query.equalTo("user", user);
    query.greaterThan("qty", 0);
    query.find({
        success: function(carts) {
            var cartTotal = 0;
            _.each(carts, function(cart) {
                    var sutotal = cart.get('price') * cart.get('qty');
                    cartTotal = cartTotal + sutotal;
                });
            
            if (total == cartTotal) {
                
                var timeStamp = Math.floor(Date.now());
                var amount = total * 100.0;
                const http = require('http');
                http.get('http://10.1.0.153/?merId=610401165732001&orderId=' + timeStamp  + '&txnAmt=' + amount, (resp) => {
                  let data = '';

                  // A chunk of data has been recieved.
                  resp.on('data', (chunk) => {
                    data += chunk.toString("utf8")
                  });


                  // The whole response has been received. Print out the result.
                  resp.on('end', () => {
                      var json = JSON.parse(data);
                      
                 
     var PreAuth = Parse.Object.extend("PreAuth");
                      var preAuth = new PreAuth();
                      preAuth.set('user', user);
                      preAuth.set('uuid', uuid);
                      preAuth.set('total', total);
                      preAuth.set('paymethod', paymethod);
                      preAuth.set('tn', json.tn);
                      preAuth.set('orderId', timeStamp);
                      preAuth.save(null, {
                                useMasterKey: true,
                                success: function(result) {
                                    response.success({"code":0, "message":"verify done", "tn" : json.tn});
                                       
                                  },
                                  error: function(user, error) {
                                        response.error("ERR-PREAUTH-SAVE");
                                  }
                                });
                        });


                }).on("error", (err) => {
                    response.error("Error: " + err.message);
                });

            }
            else {
                response.error("ERR-VERIFY-FAIL"+cartTotal);
            }
            
        },
        error: function(error) {
            response.error("ERR-CART-NOTFOUND");
        }
    });
    
});


Parse.Cloud.define('pay', function(request, response) {
    
    var userId = request.params.userId;
    var userId = request.params.userId;
    var uuid = request.params.uuid;
    var total = request.params.total;
    var lastName = request.params.lastName;
    var firstName = request.params.firstName;
    var paymethod = request.params.paymethod;
    var shipping = request.params.shipping;
    var location = request.params.location; // lat,lon
    var sign = request.params.sign;
    
    
    if (userId && uuid && total && lastName & firstName && paymethod && sign) {
        response.error("ERR-MISSING-PARAM");
        return;
    }
    
    var user = new Parse.User();
    user.id = userId;
    
    // query pre-auth to check
    var queryPre = new Parse.Query("PreAuth");
    queryPre.equalTo("uuid", uuid);
    queryPre.find({
        success: function(preauths) {
            
            // check pre-auth is correct
            if (preauths.length == 1) {
                
                // query user cart to add to user order
                var query = new Parse.Query("UserCart");
                query.equalTo("user", user);
                query.greaterThan("qty", 0);
                query.find({
                    success: function(carts) {
                        // create user order
                        var UserOrder = Parse.Object.extend("UserOrder");
                        var userOrder = new UserOrder();
                        userOrder.set('user', user);
                        userOrder.set('lastName', lastName);
                        userOrder.set('firstName', firstName);
                        userOrder.set('paymethod', paymethod);
                        userOrder.set('status', 'Ordered');
                        if (shipping) {
                            userOrder.set('shipping', shipping); 
                        }
                       
                        userOrder.set('sign', sign);
                        if (location) {
                            var arrLocation = location.split(",");
                            if (arrLocation.length > 1) {
                                var point = new Parse.GeoPoint({latitude: Number(arrLocation[0]), longitude: Number(arrLocation[1])});
                                userOrder.set('location', point);
                            }
                        }
                        
                        
                        
                        // mapping user cart to order detail
                        var arrOrderDetail = [];
                        _.each(carts, function(cart) {
                                var UserOrderDetail = Parse.Object.extend("UserOrderDetail");
                                var orderDetail = new UserOrderDetail();
                                orderDetail.set('userOrder', userOrder);
                                orderDetail.set('price', cart.get('price'));
                                orderDetail.set('product', cart.get('product'));
                                orderDetail.set('qty', cart.get('qty'));
                                orderDetail.set('size', cart.get('size'));
                                orderDetail.set('color', cart.get('color'));
                                arrOrderDetail.push(orderDetail);
                            });

                        // save order and order detail
                         Parse.Object.saveAll(arrOrderDetail, {
                            success: function(list) {
                              response.success({"code":0,"message":"success"});
                                
                                // remove carts
                                 _.each(carts, function(cart) {
                                            cart.destroy();
                                        });       
                                  
                                
                            },
                            error: function(error) {
                              response.error("ERR-ORDER-SAVE");
                            },
                          });
                        

                    },
                    error: function(error) {
                        response.error("ERR-CART-NOTFOUND");
                    }
                });
            }
            else {
                response.error("ERR-PreAuth-NOTFOUND | ERR-PreAuth-MORE");
            }
            
            
        },
        error: function(error) {
            response.error("ERR-PreAuth-NOTFOUND");
        }
    });
    
    
    
});



Parse.Cloud.define('my_purchase', function(request, response) {
    
    var userId = request.params.userId;
    var skip = request.params.skip ? request.params.skip : 0;
    var limit = request.params.limit ? request.params.limit : 10;
    
    
    var user = new Parse.User();
    user.id = userId;
    
    var query = new Parse.Query("UserOrder");
    query.equalTo("user", user);
    query.descending("createdAt");
    query.limit(limit);
    query.skip(skip);
    query.find({
        success: function(orders) {
            response.success(orders);
        },
        error: function(error) {
            response.error("ERR-OFFER-NOTFOUND");
        }
    });
           
});



Parse.Cloud.define('my_purchase_detail', function(request, response) {
    var orderId = request.params.orderId;
    
    if (!orderId) {
        response.error("ERR-MISSING-PARAM");
        return;    
    }
    
    var UserOrder = Parse.Object.extend("UserOrder");
    var userOrder = new UserOrder();
    userOrder.id = orderId
    
    var arrResult = [];
    
   var query = new Parse.Query("UserOrderDetail");
    query.equalTo("userOrder", userOrder);
    query.greaterThan("qty", 0);
    query.include("product");
    query.find().then(function(carts){
      var promises = [];
        
        if (carts.length == 0) {
              return Parse.Promise.when(promises); 
        }
        
      _.each(carts, function(cart){
          
          
          
            var arrImage = [];
          
            // get product images
            var product = cart.get('product');
            if (product) {
                var image_query = new Parse.Query("ProductImage");
                image_query.equalTo("product", product);
                image_query.select('image');
                var promise1 = image_query.find().then(function(images){
                    
                    
                    for(var i = 0; i < images.length;i++){
                        arrImage.push(images[i].get('image').url());
                    }
            
                });

                promises.push(promise1);
                arrResult.push(manipulateCartProductAndImages(cart, product, arrImage));
            }
            

      });

      //if returning another promise, the ".then" won't execute until it completes.
      return Parse.Promise.when(promises); 
    }).then(function(){   
      response.success(arrResult);
    });
    
});

// my new update function
Parse.Cloud.define('get_product_category', function(request, response) {

    var query = new Parse.Query("ProductCategory");
    query.limit(4);
    query.descending("createdAt");
    query.find({
        success: function(categories) {

            response.success(categories);

        },
        error: function(error) {
            response.error("ERR-PRODUCTCAT-NOTFOUND");
        }
    });
});

Parse.Cloud.define('getFavoriteShop', function(request, response) {
    var userId = request.params.userId;
    var user = new Parse.User();
    user.id = userId;


    var shopArr = [];

    var allShop = new Parse.Query("Shop");
    allShop.equalTo("status", true);
    allShop.limit(1000);
    allShop.find().then(function(shopFavorite){
        var promise = [];
        if (shopFavorite.length == 0) {
            return Parse.Promise.when(promise);
        }
        _.each(shopFavorite, function(shop){
            var shop1 = [];

            var shopId = shop.id;
            var Shop = Parse.Object.extend("Shop");
            var shopObj = new Shop();
            shopObj.id = shopId;

            var shopFollow = new Parse.Query("ShopFollow");
            shopFollow.equalTo("user", user);
            shopFollow.equalTo("shop", shopObj);
            var promise1 = shopFollow.find(function (shopflw) {
                //console.log(shopflw.length);
                var s = {
                    shoId: shop.id,
                    shopName: shop.get("name"),
                    shopLogo: shop.get("logo").url(),
                    isFavorite: shopflw.length > 0 ? true : false
                };
                //console.log(s);
                shop1.push(s);
            });
            promise.push(promise1);
            shopArr.push(shop1);
        });
        return Parse.Promise.when(promise);
    }).then(function () {
        response.success(convertArr2DTo1D(shopArr));
    });

});

Parse.Cloud.define('my_coupon', function(request, response){
    var userId = request.params.userId;
    var user = new Parse.User();
    user.id = userId;

    var query = new Parse.Query("UserCoupon");
    query.include(["user", "coupon"]);
    query.equalTo("user", user);
    query.find().then(function(coupon){
        let myCoupon = coupon.filter(data => data.name > new Date());
        response.success(myCoupon);
    });

});

Parse.Cloud.define('filterShopByCate', function(request, response) {
    var shopCate = request.params.shopCate;
    // var ShopCategory = Parse.Object.extend('ShopCategory');
    // var shopCateObj = new ShopCategory();

    var shopArr = [];

    var promise = [];
    if (shopCate.length == 0) {
        return Parse.Promise.when(promise);
    }
    for(let i = 0; i < shopCate.length; i ++){
        (function (e) {
            var shop1 = [];
            var ShopCategory = Parse.Object.extend('ShopCategory');
            var shopCateObj = new ShopCategory();
            shopCateObj.id = shopCate[i];
            var query = new Parse.Query('Shop');
            query.include('category');
            query.equalTo('category', shopCateObj);
            var promise1 = query.find().then(function (shop) {
                shop1.push(shop);
            });
            promise.push(promise1);
            shopArr.push(shop1);
            return Parse.Promise.when(promise);
        })(console.log(shopArr));
    }

});



/// private method
function manipulateProductAndImages(product, images) {
    var dictProduct = {};
    dictProduct['objectId'] = product.id;
    dictProduct['name'] = product.get('name');
    dictProduct['category'] = product.get('category');
    dictProduct['code'] = product.get('code');
    dictProduct['price'] = product.get('price');
    dictProduct['desc'] = product.get('desc');
    dictProduct['related'] = product.get('related');
    dictProduct['size'] = product.get('size');
    dictProduct['color'] = product.get('color');
    dictProduct['images'] = images;
    dictProduct['available'] = product.get('available');
    return dictProduct;
}

function manipulateCartProductAndImages(cart, product, images) {
    var dictProduct = manipulateProductAndImages(product, images);
    var dictCart = {};
    dictCart['product'] = dictProduct;
    dictCart['qty'] = cart.get('qty');
    dictCart['price'] = cart.get('price');
    dictCart['color'] = cart.get('color');
    dictCart['size'] = cart.get('size');
    dictCart['objectId'] = cart.id;
    return dictCart;
}
function convertArr2DTo1D(arr) {
    var array = [];
    for(let x = 0; x < arr.length; x ++){
        for(let y = 0; y < arr[x].length; y ++){
            array.push(arr[x][y])
        }
    }
    return array;
}
function manipulateRestaurant() {

}